const config = {
  db: {
    name: 'moviebot',
    sessionStateCollection: 'sessionStateCollection',
    url: 'mongodb://localhost:27017',
    userCollection: 'userStateCollection',
  },
};

export default config;
