export const commandId = '!';
export const nominationLimit = 1;
export const voterLimit = 1;

export const messages = {
  alreadyNominated: (item: string) => `${item} already nominated`,
  alreadyRegistered: 'You are already registered to vote in this election.',
  alreadyVoted: (item: string) => `You have already voted for '${item}'.`,
  commandNotFound: (command: string) => `Command could not be found: '${command}'`,
  dateSuccessfullySet: (newDate: string) => `New movie night date successfully set for: ${newDate}`,
  invalidArgument: (command: string) => `You did not enter a valid argument with the command: '${command}'`,
  isElection: 'There is an election currently in progress.',
  listNominations: (nominations: string) => `New round of voting on the following nominations:\n
IMDB ID: Movie Title (Year). Ref: link to IMDB page.
${nominations}`,
  movieChosen: (movieName: string) => `Movie chosen: '${movieName}'`,
  movieNotFound: (query?: string) => query ? `Movie not found: '${query}'` : 'Movie not found',
  movieNotFoundFromId: (id: string) => `Movie not found from id: '${id}'`,
  newMovieVoteStarted: `New movie vote started. Please '${commandId}Register' to vote.`,
  newSeasonStarted: `New season started. Please '${commandId}Register' to vote.`,
  noElection: 'There is no election in progress.',
  noMovieNight: 'There is no upcoming movie night.',
  noNomination: 'Cannot nominate for vote.',
  noRegistration: 'Cannot register to vote.',
  nominationAccepted: `Your nomination has been accepted.
Use '${commandId}EndNomination' command to move on to voting phase.`,
  nominationPhaseComplete: `Nomination phase now complete. Moving on to the voting round.
Use '${commandId}Vote %IMDB_ID%' to vote.
Use '${commandId}SearchMovie %MOVIE_TITLE%' to look up movies and their IMDB ID.`,
  notEnoughNominations: (count: string, limit: string) =>
    `Not enough nominations. Currently there are ${count} and at least ${limit} are needed.`,
  notEnoughVoters: (count: string, limit: string) =>
    `Not enough voters. Currently there are ${count} and at least ${limit} are needed.`,
  notEnoughVotes: 'You do not have any votes left.',
  notInPhase: (phase: string) => `Voting is not in the ${phase} phase.`,
  notRegisteredToVote: 'You are not registered to vote.',
  notVotable: (item: string) => `${item} is not an available item to vote on.`,
  registeredToVote: `You are registered to vote.
Use command \'${commandId}EndRegistration\' to continue to nomination phase.`,
  registrationPhaseComplete: (genre: string) => `Registration phase complete. Moving on to the nominations.
Use '${commandId}Nominate %IMDB_ID%' command to nominate movies for genre: ${genre}`,
  resetAllSuccessful: 'Successfully reset the server state.',
  resetMovieNightSuccessful: 'Successfully reset the movie night.',
  resetPreferencesSuccessful: 'Successfully reset the server preferences.',
  resetVotingSuccessful: 'Successfully reset the server voting session.',
  searchFailed: 'Search failed. Try again.',
  seasonInProgress: 'Season currently in progress. End this one before beginning a new one',
  seasonNotInProgress: 'Season not currently in progress. Start a new season before continuing.',
  voteAccepted: (votesRemaining: number) => `Your vote has been cast.
You have ${votesRemaining} votes remaining.`,
  votingComplete: 'Voting is now complete.',
  yearNotValid: (query: string) => `Year is not valid: '${query}'`,
};
