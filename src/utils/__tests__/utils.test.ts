import { commandId, handleMessage } from '..';

describe('messageHandler', () => {
  it('should reject messages without command id at beginning of content', () => {
    const input = 'test';
    const expected = [];
    const actual = handleMessage(input);

    expect(actual).toEqual(expected);
  });

  it('should return command with command id at the front', () => {
    const input = `${commandId}test`;
    const expected = ['test'];
    const actual = handleMessage(input);

    expect(actual).toEqual(expected);
  });

  it('should return command when there is a parameter seperated by a space', () => {
    const input = `${commandId}test param`;
    const expected = ['test', 'param'];
    const acutal = handleMessage(input);

    expect(acutal).toEqual(expected);
  });
});
