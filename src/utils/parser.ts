import { commandId } from './constants';

export function handleMessage(message: string): string[] {
  const messageHasCommandIdAtBeginning = message.indexOf(commandId) === 0;

  if (messageHasCommandIdAtBeginning) {
    const breakdown = message.toLocaleLowerCase().substr(commandId.length).split(' ') || [];
    return breakdown.filter((x) => {
      return x.trim().length > 0;
    });
  }
  return [];
}
