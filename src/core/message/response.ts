export interface IResponseMessage {
  broadcast?: {
    channelId: string;
    message: string;
  };
  reply?: string;
  success: boolean;
}


