import { IServerStateLocator } from '.';

export interface IUserState extends IUserLocator {
  registration?: IServerStateLocator;
}

export interface IUserLocator {
  locatorId: string;
}
