export interface IServerStateLocator {
  locatorId: string;
}

export interface IServerState extends IServerStateLocator {
  currentMovieNight?: IMovieNight;
  currentSeason?: ISeason;
  preferences?: IPreferences;
  voting?: IVoting;
}

export interface IPreferences {
  allowStackedVoting?: boolean;
  nominationLimit?: number;
  voterLimit?: number;
}

export interface IArchive {
  genres?: INomination[];
  locator: IServerStateLocator;
  movieNights?: IMovieNight[];
  movies?: IMovie[];
  seasons?: ISeason[];
}

export interface ISeason {
  genres: INomination[];
  start: Date;
}

export interface IIdenifiable {
  id: string;
}

export interface INomination {
  name: string;
}

export interface IMovieNight {
  genre: INomination;
  movie: IMovie;
  night: Date;
}

export interface IMovie extends IIdenifiable, INomination {
  genres: INomination[];
  year: number;
}

export interface IUser {
  id: string;
}

export interface IVoting {
  channelId: string;
  chosen?: IIdenifiable;
  genre?: INomination;
  nominations: IVoteable[];
  phase: VotingPhase;
  round: number;
  schema: VotingSchemas;
  voters: IVoter[];
  votingRounds?: IRound[];
}

export interface IVoter {
  points: number;
  user: IUser;
}

export interface IRound {
  chosen?: IVoteable;
  items: IVoteable[];
  voters: IVoter[];
}

export interface IVoteable {
  item: IIdenifiable;
  voters?: IVoter[];
  votes: number;
}

export enum VotingPhase {
  complete = 'complete',
  nomination = 'nomination',
  registration = 'registration',
  voting = 'voting',
}

export enum VotingSchemas {
  genre,
  movie,
}
