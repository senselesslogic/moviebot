import {
  IMovieLocator,
  IMovieRepository,
  IStateRepository,
} from '../../database/interfaces';
import { execute } from '../commands';
import { ICommand } from '../commands/commands';
import {
  IMovie,
  IServerState,
  IServerStateLocator,
  IUserLocator,
  IUserState,
} from './../interfaces';

describe('Movie Voting phases', () => {
  it('should only progress when phases are complete and validators pass', async () => {
    let serverState: IServerState = {
      locatorId: "1",
      preferences: {
        nominationLimit: 1,
        voterLimit: 1,
      },
    };

    let userState: IUserState = {
      locatorId: "a",
    };

    const testMovie: IMovie = {
      genres: [],
      id: "testMovieId",
      name: "testMovie",
      year: 1000,
    };

    const movieRepoMock = jest.fn<IMovieRepository, []>(() => ({
      get: (locator: IMovieLocator) => {
        if (locator.id === testMovie.id) {
          return testMovie;
        }
        throw new Error('movie not found');
      },
      ...movieRepoMock,
    }));
    const movieRepository: IMovieRepository = new movieRepoMock();

    const serverRepoMock = jest.fn<IStateRepository, []>(() => ({
      getServerState: (locator: IServerStateLocator) => {
        if (locator.locatorId === serverState.locatorId) {
          return serverState;
        }
        throw new Error('server state not found');
      },
      getUserState: (locator: IUserLocator) => {
        if (locator.locatorId === userState.locatorId) {
          return userState;
        }
        throw new Error('user state not found');
      },
      setServerState: (state: IServerState) => {
        if (state.locatorId === serverState.locatorId) {
          serverState = state;
          return serverState;
        }
        throw new Error('server state not found');
      },
      setUserState: (state: IUserState) => {
        if (state.locatorId === userState.locatorId) {
          userState = state;
          return userState;
        }
        throw new Error('server state not found');
      },
      ...serverRepoMock,
    }));
    const serverRepository: IStateRepository = new serverRepoMock();

    const baseCommand = {
      broadcastId: "0",
      movieRepo: movieRepository,
      repo: serverRepository,
      serverLocator: { locatorId: "1" },
      user: { id: "a" },
    };

    const endNominationCom: ICommand = {
      userCommand: ["endNomination"],
      ...baseCommand,
    };
    const endRegistrationCom: ICommand = {
      userCommand: ["endRegistration"],
      ...baseCommand,
    };
    const nominateCom: ICommand = {
      userCommand: ["nominate", testMovie.id],
      ...baseCommand,
    };
    const registerCom: ICommand = {
      userCommand: ["register"],
      ...baseCommand,
    };
    const startMovieVoteCom: ICommand = {
      userCommand: ["startMovieVote"],
      ...baseCommand,
    };
    const voteCom: ICommand = {
      userCommand: ["vote", testMovie.id],
      ...baseCommand,
    };

    // Need to start movie vote before registration
    let endNominationResponse = await execute(endNominationCom);
    let endRegistrationResponse = await execute(endRegistrationCom);
    let nominateResponse = await execute(nominateCom);
    let registerResponse = await execute(registerCom);
    let voteResponse = await execute(voteCom);
    let startMoviteVoteResponse = await execute(startMovieVoteCom);

    expect(endRegistrationResponse.success).toBeFalsy();
    expect(endNominationResponse.success).toBeFalsy();
    expect(nominateResponse.success).toBeFalsy();
    expect(registerResponse.success).toBeFalsy();
    expect(voteResponse.success).toBeFalsy();
    expect(startMoviteVoteResponse.success).toBeTruthy();

    // Need to register before ending registration
    endNominationResponse = await execute(endNominationCom);
    endRegistrationResponse = await execute(endRegistrationCom);
    nominateResponse = await execute(nominateCom);
    startMoviteVoteResponse = await execute(startMovieVoteCom);
    voteResponse = await execute(voteCom);
    registerResponse = await execute(registerCom);

    expect(endNominationResponse.success).toBeFalsy();
    expect(endRegistrationResponse.success).toBeFalsy();
    expect(nominateResponse.success).toBeFalsy();
    expect(startMoviteVoteResponse.success).toBeFalsy();
    expect(voteResponse.success).toBeFalsy();
    expect(registerResponse.success).toBeTruthy();

    // Need to end registration before nomination
    endNominationResponse = await execute(endNominationCom);
    nominateResponse = await execute(nominateCom);
    registerResponse = await execute(registerCom);
    startMoviteVoteResponse = await execute(startMovieVoteCom);
    voteResponse = await execute(voteCom);
    endRegistrationResponse = await execute(endRegistrationCom);

    expect(endNominationResponse.success).toBeFalsy();
    expect(nominateResponse.success).toBeFalsy();
    expect(registerResponse.success).toBeFalsy();
    expect(startMoviteVoteResponse.success).toBeFalsy();
    expect(voteResponse.success).toBeFalsy();
    expect(endRegistrationResponse.success).toBeTruthy();

    // Need to nominate before ending nominations
    endNominationResponse = await execute(endNominationCom);
    endRegistrationResponse = await execute(endRegistrationCom);
    registerResponse = await execute(registerCom);
    startMoviteVoteResponse = await execute(startMovieVoteCom);
    voteResponse = await execute(voteCom);
    nominateResponse = await execute(nominateCom);

    expect(endNominationResponse.success).toBeFalsy();
    expect(endRegistrationResponse.success).toBeFalsy();
    expect(registerResponse.success).toBeFalsy();
    expect(startMoviteVoteResponse.success).toBeFalsy();
    expect(voteResponse.success).toBeFalsy();
    expect(nominateResponse.success).toBeTruthy();

    // Need to end nomination before voting
    endRegistrationResponse = await execute(endRegistrationCom);
    nominateResponse = await execute(nominateCom);
    registerResponse = await execute(registerCom);
    startMoviteVoteResponse = await execute(startMovieVoteCom);
    voteResponse = await execute(voteCom);
    endNominationResponse = await execute(endNominationCom);

    expect(endRegistrationResponse.success).toBeFalsy();
    expect(nominateResponse.success).toBeFalsy();
    expect(registerResponse.success).toBeFalsy();
    expect(startMoviteVoteResponse.success).toBeFalsy();
    expect(voteResponse.success).toBeFalsy();
    expect(endNominationResponse.success).toBeTruthy();

    // Movie chosen
    endRegistrationResponse = await execute(endRegistrationCom);
    endNominationResponse = await execute(endNominationCom);
    nominateResponse = await execute(nominateCom);
    registerResponse = await execute(registerCom);
    startMoviteVoteResponse = await execute(startMovieVoteCom);
    voteResponse = await execute(voteCom);

    expect(endNominationResponse.success).toBeFalsy();
    expect(endRegistrationResponse.success).toBeFalsy();
    expect(nominateResponse.success).toBeFalsy();
    expect(registerResponse.success).toBeFalsy();
    expect(startMoviteVoteResponse.success).toBeFalsy();
    expect(voteResponse.success).toBeTruthy();
  });
});
