import {
  endNomination,
  endRegistration,
  getMovie,
  nominate,
  register,
  searchMovie,
  startMovieVote,
  vote,
} from '.';
import {
  IServerStateLocator,
  IUser,
} from '..';
import { IMovieRepository, IStateRepository } from '../../database';
import { messages } from '../../utils';
import { IResponseMessage } from '../message/response';
import { IServerState } from './../interfaces/sessionState';

export interface ICommand {
  broadcastId: string;
  userCommand: string[];
  movieRepo: IMovieRepository;
  repo: IStateRepository;
  serverLocator: IServerStateLocator;
  user: IUser;
}

export const commands: { [key: string]: (com: ICommand) => Promise<IResponseMessage> } = {
  endnomination: (com: ICommand) => endNomination(com),
  endregistration: (com: ICommand) => endRegistration(com),
  getmovie: (com: ICommand) => getMovie(com),
  help: (com: ICommand) => help(com),
  info: (com: ICommand) => info(com),
  nominate: (com: ICommand) => nominate(com),
  ping: (com: ICommand) => ping(com),
  register: (com: ICommand) => register(com),
  resetall: (com: ICommand) => resetAll(com),
  resetmovienight: (com: ICommand) => resetMovieNight(com),
  resetpreferences: (com: ICommand) => resetPreferences(com),
  resetvoting: (com: ICommand) => resetVoting(com),
  searchmovie: (com: ICommand) => searchMovie(com),
  startmovievote: (com: ICommand) => startMovieVote(com),
  vote: (com: ICommand) => vote(com),
  // startseasonvote: (com: ICommand) => startSeasonVote(com), // removing until I decide to inc season
  // voteMute: (com: ICommand) => voteMute(com),
  // setDate: (com: ICommand) => vote(com),
  // veto: (com: ICommand) => vote(com),
};

export async function execute(com: ICommand): Promise<IResponseMessage> {
  const input = com.userCommand;
  if (input && input.length > 0) {
    const base = input[0].toLocaleLowerCase();
    if (commands.hasOwnProperty(base)) {
      return commands[base](com);
    }
  }
  return notFound(com);
}

async function notFound(com: ICommand): Promise<IResponseMessage> {
  const reply = messages.commandNotFound(com.userCommand[0]);
  return {
    reply,
    success: true,
  };
}

async function ping(com: ICommand): Promise<IResponseMessage> {
  const reply = 'Pong!';
  return {
    reply,
    success: true,
  };
}

async function info(com: ICommand): Promise<IResponseMessage> {
  const state = await com.repo.getServerState(com.serverLocator);
  return {
    reply: JSON.stringify(state),
    success: true,
  };
}

async function resetAll(com: ICommand): Promise<IResponseMessage> {
  const state: IServerState = await com.repo.getServerState(com.serverLocator);

  state.currentMovieNight = undefined;
  state.currentSeason = undefined;
  state.preferences = undefined;
  state.voting = undefined;

  await com.repo.setServerState(state);

  return {
    reply: messages.resetAllSuccessful,
    success: true,
  };
}

async function resetVoting(com: ICommand): Promise<IResponseMessage> {
  const state: IServerState = await com.repo.getServerState(com.serverLocator);

  state.voting = undefined;

  await com.repo.setServerState(state);

  return {
    reply: messages.resetVotingSuccessful,
    success: true,
  };
}

async function resetMovieNight(com: ICommand): Promise<IResponseMessage> {
  const state: IServerState = await com.repo.getServerState(com.serverLocator);

  state.currentMovieNight = undefined;

  await com.repo.setServerState(state);

  return {
    reply: messages.resetMovieNightSuccessful,
    success: true,
  };
}

async function resetPreferences(com: ICommand): Promise<IResponseMessage> {
  const state: IServerState = await com.repo.getServerState(com.serverLocator);

  state.preferences = undefined;

  await com.repo.setServerState(state);

  return {
    reply: messages.resetPreferencesSuccessful,
    success: true,
  };
}

async function help(com: ICommand): Promise<IResponseMessage> {
  const keys = Object.keys(commands);

  return {
    reply: `Available commands:\n${keys.join("\n")}`,
    success: true,
  };
}
