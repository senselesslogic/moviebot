import { sort } from 'timsort';
import { ICommand, Validator, VotingPhase, VotingValidators } from '..';
import { messages } from '../../utils';
import { IResponseMessage } from '../message/response';
import { IRound, IServerState, IUser, IVoteable, IVoter, VotingSchemas } from './../interfaces/sessionState';
import { IUserState } from './../interfaces/userState';
import { MovieValidators } from './../validators/movies';

// TODO: add a way to start season without voting.
// TODO: refactor to use functions on a state object instead of directly messing with state in the command execution

export async function startSeasonVote(com: ICommand): Promise<IResponseMessage> {
  const state = await com.repo.getServerState(com.serverLocator);
  const result = new Validator(state)
    .addValidator(VotingValidators.hasNoSeason)
    .addValidator(VotingValidators.isNotVoting)
    .runValidation();

  if (result.success) {
    state.voting = {
      channelId: com.broadcastId,
      nominations: [],
      phase: VotingPhase.registration,
      round: 0,
      schema: VotingSchemas.genre,
      voters: [],
    };

    await com.repo.setServerState(state);

    return {
      reply: messages.newSeasonStarted,
      success: result.success,
    };
  } else {
    return {
      reply: result.message,
      success: result.success,
    };
  }
}

export async function startMovieVote(com: ICommand): Promise<IResponseMessage> {
  const state = await com.repo.getServerState(com.serverLocator);
  const result = new Validator(state)
    // .addValidator(VotingValidators.hasSeason)
    .addValidator(VotingValidators.isNotVoting)
    .runValidation();

  if (result.success) {
    // const pickMe = Math.floor(Math.random() * state.currentSeason.genres.length);
    // const genre = state.currentSeason.genres[pickMe];

    // state.currentSeason.genres = state.currentSeason.genres.filter((g) => {
    //   return g !== genre;
    // });

    const genre = {
      name: com.userCommand.length > 1 ? com.userCommand.slice(1).join(" ") : "Any",
    };

    state.voting = {
      channelId: com.broadcastId,
      genre,
      nominations: [],
      phase: VotingPhase.registration,
      round: 0,
      schema: VotingSchemas.movie,
      voters: [],
    };

    await com.repo.setServerState(state);

    return {
      reply: messages.newMovieVoteStarted,
      success: result.success,
    };
  } else {
    return {
      reply: result.message,
      success: result.success,
    };
  }
}

export async function register(com: ICommand): Promise<IResponseMessage> {
  const state = await com.repo.getServerState(com.serverLocator);
  const result = new Validator(state)
    .addValidator(VotingValidators.isVoting)
    .addValidator(VotingValidators.isInVotingPhase(VotingPhase.registration))
    .addValidator(VotingValidators.userIsNotRegistered(com.user))
    .runValidation();

  if (result.success) {
    state.voting.voters.push({
      points: 0,
      user: com.user,
    });

    let userState: IUserState = await com.repo.getUserState({locatorId: com.user.id});
    if (!userState) {
      userState = {
        locatorId: com.user.id,
        registration: com.serverLocator,
      }
    }
    await com.repo.setServerState(state);

    return {
      reply: messages.registeredToVote,
      success: result.success,
    };
  } else {
    return {
      reply: result.message,
      success: result.success,
    };
  }
}

export async function endRegistration(com: ICommand): Promise<IResponseMessage> {
  const state = await com.repo.getServerState(com.serverLocator);
  const result = new Validator(state)
    .addValidator(VotingValidators.isVoting)
    .addValidator(VotingValidators.isInVotingPhase(VotingPhase.registration))
    .addValidator(VotingValidators.hasMinimumVoters)
    .runValidation();

  if (result.success) {
    state.voting.phase = VotingPhase.nomination;

    await com.repo.setServerState(state);

    return {
      broadcast: {
        channelId: state.voting.channelId,
        message: messages.registrationPhaseComplete(state.voting.genre.name),
      },
      success: result.success,
    };
  } else {
    return {
      reply: result.message,
      success: result.success,
    };
  }
}

export async function nominate(com: ICommand): Promise<IResponseMessage> {
  const state = await com.repo.getServerState(com.serverLocator);
  const nomination = { id: com.userCommand[1] };
  const stateValidator = new Validator(state)
    .addValidator(VotingValidators.isVoting)
    .addValidator(VotingValidators.isInVotingPhase(VotingPhase.nomination))
    .addValidator(VotingValidators.userIsRegistered(com.user))
    .addValidator(MovieValidators.isMovie(nomination.id, com.movieRepo))
    .addValidator(VotingValidators.notNominated(nomination));
    // TODO: add nomination limit.

  const result = stateValidator.runValidation();

  if (result.success) {
    state.voting.nominations.push({
      item: {
        id: com.userCommand[1],
      },
      voters: [],
      votes: 0,
    });

    await com.repo.setServerState(state);

    return {
      reply: messages.nominationAccepted,
      success: result.success,
    };
  } else {
    return {
      reply: result.message,
      success: result.success,
    };
  }
}

export async function endNomination(com: ICommand): Promise<IResponseMessage> {
  const state = await com.repo.getServerState(com.serverLocator);
  const result = new Validator(state)
    .addValidator(VotingValidators.isVoting)
    .addValidator(VotingValidators.isInVotingPhase(VotingPhase.nomination))
    .addValidator(VotingValidators.hasMinimumNominations)
    .runValidation();

  if (result.success) {
    state.voting.phase = VotingPhase.voting;

    const voterSetup = [];
    const voterPoints = calculateVotingPoints(
      state.voting.voters.length,
      state.voting.nominations.length);

    state.voting.voters.forEach((voter) => {
      voterSetup.push({
        points: voterPoints,
        user: voter.user,
      });
    }, voterSetup);

    state.voting.votingRounds = [{
      items: state.voting.nominations,
      voters: voterSetup,
    }];

    await com.repo.setServerState(state);

    const nominationList = await Promise.all(state.voting.nominations.map(async (voteable: IVoteable) => {
      const movie = await com.movieRepo.get({id: voteable.item.id});
      return generateNominationRow(movie);
    }));

    return {
      broadcast: {
        channelId: state.voting.channelId,
        message: messages.listNominations(nominationList.join("\n")),
      },
      reply: messages.nominationPhaseComplete,
      success: result.success,
    };
  } else {
    return {
      reply: result.message,
      success: result.success,
    };
  }
}

function calculateVotingPoints(numberOfVoters: number, numberOfVotables: number): number {
  if (numberOfVotables <= numberOfVoters) {
    return 1;
  }
  return Math.ceil(Math.ceil(numberOfVotables / numberOfVoters) / 2);
}

export async function vote(com: ICommand): Promise<IResponseMessage> {
  const state = await com.repo.getServerState(com.serverLocator);
  const item = { id: com.userCommand[1] };
  const currentUser = com.user;

  const result = new Validator(state)
    .addValidator(VotingValidators.isVoting)
    .addValidator(VotingValidators.isInVotingPhase(VotingPhase.voting))
    .addValidator(VotingValidators.userIsRegistered(currentUser))
    .addValidator(VotingValidators.hasVotes(currentUser))
    .addValidator(VotingValidators.isVoteable(item))
    .addValidator(VotingValidators.checkStackedVoting(item, com.user))
    .runValidation();

  if (result.success) {
    const round = state.voting.votingRounds[state.voting.round];
    const votedItem = round.items.find((votable) => votable.item.id === item.id);
    const itemVoters = votedItem.voters;
    const roundVoters = round.voters;
    const currentRoundUser = roundVoters.find(
      (voter) => {
        return voter.user.id === currentUser.id;
      },
    );

    decrementRoundVoterVotes(currentRoundUser, roundVoters);
    incrementItemVotes(itemVoters, currentUser);
    votedItem.votes++;

    if (roundVoters.length === 0) {
      const nextRoundPicks = calculateVotes(round.items);

      if (nextRoundPicks.length === 1) {
        state.voting.voters.forEach(async (voter) => {
          const userState = await com.repo.getUserState({locatorId: voter.user.id});
          userState.registration = undefined;
          await com.repo.setUserState(userState);
        });

        round.chosen = nextRoundPicks[0];
        await createMovieNight(round);
        const broadcastChannelId = state.voting.channelId;

        state.voting = undefined;
        await com.repo.setServerState(state);

        const movieDisplayTitle = `${state.currentMovieNight.movie.name} (${state.currentMovieNight.movie.year})`;

        return {
          broadcast: {
            channelId: broadcastChannelId,
            message: messages.movieChosen(movieDisplayTitle),
          },
          reply: messages.votingComplete,
          success: result.success,
        };
      } else {
        const voterSetup = [];

        const voterPoints = calculateVotingPoints(
          state.voting.voters.length,
          nextRoundPicks.length);

        state.voting.voters.forEach((voter) => {
          voterSetup.push({
            points: voterPoints,
            user: voter.user,
          });
        }, voterSetup);

        nextRoundPicks.forEach((voteable: IVoteable) => {
          voteable.voters = [];
          voteable.votes = 0;
        });

        state.voting.round++;
        state.voting.votingRounds.push({
          items: nextRoundPicks,
          voters: voterSetup,
        });

        const nominationList = await Promise.all(
            state.voting.votingRounds[state.voting.round].items.map(async (voteable: IVoteable) => {
          const movie = await com.movieRepo.get({id: voteable.item.id});
          return generateNominationRow(movie);
        }));

        await com.repo.setServerState(state);

        return {
          broadcast: {
            channelId: state.voting.channelId,
            message: messages.listNominations(nominationList.join("\n")),
          },
          reply: messages.nominationPhaseComplete,
          success: result.success,
        };
      }
    }

    await com.repo.setServerState(state);

    return {
      reply: messages.voteAccepted(currentRoundUser.points),
      success: result.success,
    };
  }
  return {
    reply: result.message,
    success: result.success,
  };

  async function createMovieNight(round: IRound) {
    const movieDetails = await com.movieRepo.get({ id: round.chosen.item.id });
    state.currentMovieNight = {
      genre: state.voting.genre,
      movie: {
        genres: [state.voting.genre],
        id: round.chosen.item.id,
        name: movieDetails.title,
        year: movieDetails.year,
      },
      night: new Date(),
    };
  }

  function decrementRoundVoterVotes(currentRoundUser: IVoter, roundVoters: IVoter[]) {
    currentRoundUser.points--;
    if (currentRoundUser.points <= 0) {
      roundVoters.splice(roundVoters.indexOf(currentRoundUser), 1);
    }
  }

  function incrementItemVotes(itemVoters: IVoter[], targetUser: IUser) {
    const foundVoter = itemVoters.find((voter) => voter.user.id === targetUser.id);
    if (foundVoter) {
      foundVoter.points++;
    } else {
      itemVoters.push({
        points: 1,
        user: targetUser,
      });
    }
  }
}

function generateNominationRow(movie): string {
  return `\`${movie.imdbid}\`: ${movie.name} (${movie.year}). Ref: ${movie.imdburl}`;
}

export function calculateVotes(items: IVoteable[]): IVoteable[] {
  return calculateVotesWithLimit(items, calculateLimit(items.length));
}

export function calculateVotesWithLimit(items: IVoteable[], limit: number): IVoteable[] {
  if (limit === 0) {
    return [];
  }

  if (items.length === 1) {
    return items;
  }

  sort(items, (a: IVoteable, b: IVoteable) => {
    return b.votes - a.votes;
  });

  if (items.length <= limit) {
    return items;
  }

  if (items[limit - 1].votes !== items[limit].votes) {
    return items.slice(0, limit);
  }

  if (items[0].votes === items[limit - 1].votes) {
    for (let i = limit; i < items.length; i++) {
      if (items[i].votes !== items[0].votes) {
        return items.slice(0, i);
      }
    }
    return items.slice(0, items.length);
  }

  const votesMismatchIndexDesc = () => {
    for (let i = limit - 2; i > 0; i--) {
      if (items[i].votes !== items[limit - 1].votes) {
        return i;
      }
    }
    return 0;
  };
  return items.slice(0, votesMismatchIndexDesc() + 1);
}

export function calculateLimit(count: number): number {
  return count > 10 ? 10
    : count > 6 ? 6
    : count > 3 ? 3
    : 1;
}

export async function setDate(com: ICommand): Promise<IResponseMessage> {
  const state = await com.repo.getServerState(com.serverLocator);
  const item = { id: com.userCommand[1] };
  const currentUser = com.user;

  const result = new Validator(state)
    .addValidator(VotingValidators.isNotVoting)
    .addValidator(VotingValidators.hasMovieNight)
    .addValidator(VotingValidators.hasUpcomingMovieNight)
    .runValidation();

  // TODO: add date validation and setting for movie night.

  if (result.success) {
    return {
      reply: messages.dateSuccessfullySet(""),
      success: result.success,
    };
  } else {
    return {
      reply: result.message,
      success: result.success,
    };
  }
}
