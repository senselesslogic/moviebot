import { calculateLimit, calculateVotesWithLimit } from '..';
import { IVoteable } from './../../interfaces/sessionState';

describe('calculateLimit', () => {
  it('should return 1 with with input of 0', () => {
    const input = 0;
    const expected = 1;

    const result = calculateLimit(input);

    expect(result).toEqual(expected);
  });

  it('should return 1 with with input of 1', () => {
    const input = 1;
    const expected = 1;

    const result = calculateLimit(input);

    expect(result).toEqual(expected);
  });

  it('should eventually return 1 if result is reinputed with with starting input of 100', () => {
    const input = 100;
    const expected = 1;

    let result = input;

    while (result > 1) {
      result = calculateLimit(result);
    }

    expect(result).toEqual(expected);
  });
});

describe('calculateVotesWithLimit', () => {
  it('should return empty array with 0 items', () => {
    const input: IVoteable[] = [];
    const expected = [];

    const itemLimit = 0;
    const result = calculateVotesWithLimit(input, itemLimit);

    expect(result).toEqual(expected);
  });

  it('should return empty array with 1 item and limit of 0', () => {
    const input: IVoteable[] = [
      {
        item: { id: "0" },
        votes: 1,
      },
    ];
    const expected = [];

    const itemLimit = 0;
    const result = calculateVotesWithLimit(input, itemLimit);

    expect(result).toEqual(expected);
  });

  it('should return input with 1 item and limit of 1', () => {
    const input: IVoteable[] = [
      {
        item: { id: "0" },
        votes: 1,
      },
    ];

    const itemLimit = 1;
    const result = calculateVotesWithLimit(input, itemLimit);

    expect(result).toEqual(input);
  });

  it('should return input with 1 item and limit greater than item\'s length', () => {
    const input: IVoteable[] = [
      {
        item: { id: "0" },
        votes: 1,
      },
    ];

    const itemLimit = 2;
    const result = calculateVotesWithLimit(input, itemLimit);

    expect(result).toEqual(input);
  });

  it('should return input when all items have same number of votes', () => {
    const testItem1 = {
      item: { id: "0" },
      votes: 1,
    };
    const testItem2 = {
      item: { id: "1" },
      votes: 1,
    };
    const input: IVoteable[] = [
      testItem1,
      testItem2,
    ];

    const itemLimit = 1;
    const result = calculateVotesWithLimit(input, itemLimit);

    expect(result).toEqual(input);
  });

  it('should return item with highest number of votes', () => {
    const testItem1 = {
      item: { id: "0" },
      votes: 2,
    };
    const testItem2 = {
      item: { id: "1" },
      votes: 1,
    };
    const input: IVoteable[] = [
      testItem1,
      testItem2,
    ];
    const expected: IVoteable[] = [
      testItem1,
    ];

    const itemLimit = 1;
    const result = calculateVotesWithLimit(input, itemLimit);

    expect(result).toEqual(expected);
  });

  it('should return item with highest number of votes in reverse order', () => {
    const testItem1 = {
      item: { id: "0" },
      votes: 2,
    };
    const testItem2 = {
      item: { id: "1" },
      votes: 1,
    };
    const input: IVoteable[] = [
      testItem2,
      testItem1,
    ];
    const expected: IVoteable[] = [
      testItem1,
    ];

    const itemLimit = 1;
    const result = calculateVotesWithLimit(input, itemLimit);

    expect(result).toEqual(expected);
  });

  it('should return 2 out of 3 items with highest number of votes with limit 2', () => {
    const testItem1 = {
      item: { id: "0" },
      votes: 3,
    };
    const testItem2 = {
      item: { id: "1" },
      votes: 2,
    };
    const testItem3 = {
      item: { id: "2" },
      votes: 1,
    };
    const input: IVoteable[] = [
      testItem1,
      testItem2,
      testItem3,
    ];
    const expected: IVoteable[] = [
      testItem1,
      testItem2,
    ];

    const itemLimit = 2;
    const result = calculateVotesWithLimit(input, itemLimit);

    expect(result).toEqual(expected);
  });

  it('should return item with highest number of votes excluding items where votes at limit match', () => {
    const testItem1 = {
      item: { id: "0" },
      votes: 3,
    };
    const testItem2 = {
      item: { id: "1" },
      votes: 2,
    };
    const testItem3 = {
      item: { id: "2" },
      votes: 2,
    };
    const input: IVoteable[] = [
      testItem1,
      testItem2,
      testItem3,
    ];
    const expected: IVoteable[] = [
      testItem1,
    ];

    const itemLimit = 2;
    const result = calculateVotesWithLimit(input, itemLimit);

    expect(result).toEqual(expected);
  });

  it('should return multiple items with highest number of votes', () => {
    const testItem1 = {
      item: { id: "0" },
      votes: 3,
    };
    const testItem2 = {
      item: { id: "1" },
      votes: 3,
    };
    const testItem3 = {
      item: { id: "2" },
      votes: 2,
    };
    const input: IVoteable[] = [
      testItem1,
      testItem2,
      testItem3,
    ];
    const expected: IVoteable[] = [
      testItem1,
      testItem2,
    ];

    const itemLimit = 2;
    const result = calculateVotesWithLimit(input, itemLimit);

    expect(result).toEqual(expected);
  });

  it('should return all items matching item with highest number of votes when matching limit\'s item votes', () => {
    const testItem1 = {
      item: { id: "0" },
      votes: 3,
    };
    const testItem2 = {
      item: { id: "1" },
      votes: 3,
    };
    const testItem3 = {
      item: { id: "2" },
      votes: 2,
    };
    const input: IVoteable[] = [
      testItem1,
      testItem2,
      testItem3,
    ];
    const expected: IVoteable[] = [
      testItem1,
      testItem2,
    ];

    const itemLimit = 1;
    const result = calculateVotesWithLimit(input, itemLimit);

    expect(result).toEqual(expected);
  });

  it('should return all items if limit is greater than length in order by votes', () => {
    const testItem1 = {
      item: { id: "0" },
      votes: 100,
    };
    const testItem2 = {
      item: { id: "1" },
      votes: 1,
    };
    const testItem3 = {
      item: { id: "2" },
      votes: 10,
    };
    const input: IVoteable[] = [
      testItem1,
      testItem2,
      testItem3,
    ];
    const expected: IVoteable[] = [
      testItem1,
      testItem3,
      testItem2,
    ];

    const itemLimit = 4;
    const result = calculateVotesWithLimit(input, itemLimit);

    expect(result).toEqual(expected);
  });
});
