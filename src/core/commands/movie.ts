import { messages } from '../../utils';
import { IResponseMessage } from '../message/response';
import { ICommand } from './commands';

export async function search(com: ICommand): Promise<IResponseMessage> {
  const query = com.userCommand.slice(1);
  const yearIndex = query.findIndex((x) => {
    return x === '-y';
  });
  let yearSearch = -1;
  if (yearIndex > -1 && query.length > yearIndex + 1) {
    try {
      const year = query.splice(yearIndex, 2)[1];
      yearSearch = parseInt(year, 10);
      if (isNaN(yearSearch)) {
        return {
          reply: messages.yearNotValid(year),
          success: true,
        };
      }
    } catch (e) {
      return {
        reply: messages.searchFailed,
        success: false,
      };
    }
  }
  const queryString = query.join(' ');
  try {
    const queryObj = yearSearch > -1 ? {
      title: queryString,
      year: yearSearch,
    } : {
      title: queryString,
    };
    const reply = await com.movieRepo.search(queryObj);

    return {
      reply: JSON.stringify(reply.results.slice(0, 3)),
      success: false,
    };
  } catch (e) {
    return {
      reply: messages.movieNotFound(queryString),
      success: false,
    };
  }
}

export async function get(com: ICommand): Promise<IResponseMessage> {
  const id = com.userCommand.slice(1).join('');
  try {
    const reply = await com.movieRepo.get({ id });
    return {
      reply: reply.title,
      success: true,
    };
  } catch (e) {
    return {
      reply: messages.movieNotFoundFromId(id),
      success: false,
    };
  }
}
