import { IServerState } from '..';

export class Validator {
  private validators: IValidationTask[];

  constructor(private state: IServerState) {
    this.validators = [];
  }

  public addValidator(task: IValidationTask): Validator {
    this.validators.push(task);
    return this;
  }

  public runValidation(): IValidationResult {
    let result = { success: false };
    for (const validator of this.validators) {
      result = validator.validate(this.state);
      if (!result.success) {
        break;
      }
    }
    return result;
  }
}

export interface IValidationTask {
  validate: (state?: IServerState) => IValidationResult;
}

export interface IValidationResult {
  success: boolean;
  message?: string;
}
