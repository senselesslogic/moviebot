import { IMovieRepository } from '../../database';
import { messages } from '../../utils';
import { IValidationResult } from './validator';

export const MovieValidators = {
  isMovie: (locator: string, repo: IMovieRepository) => ({ validate: () => isMovie(locator, repo)}),
};

function isMovie(locator: string, repo: IMovieRepository): IValidationResult {
  if (typeof locator === 'undefined') {
    return {
      message: messages.movieNotFound(),
      success: false,
    };
  }
  try {
    repo.get({ id: locator });
    return {
      success: true,
    };
  } catch {
    return {
      message: messages.movieNotFound(),
      success: false,
    };
  }
}
