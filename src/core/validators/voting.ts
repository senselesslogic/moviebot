import { IIdenifiable, IServerState, IUser, VotingPhase } from '..';
import { messages, nominationLimit as defaultNominationLimit, voterLimit as defaultVoterLimit } from '../../utils';
import { IValidationResult } from './validator';

export const VotingValidators = {
  checkStackedVoting: (item: IIdenifiable, user: IUser) => ({
    validate: (state: IServerState) => checkStackedVoting(state, item, user),
  }),
  hasMinimumNominations: { validate: (state: IServerState) => hasMinimumNominations(state) },
  hasMinimumVoters: { validate: (state: IServerState) => hasMinimumVoters(state) },
  hasMovieNight: { validate: (state: IServerState) => hasMovieNight(state) },
  hasNoSeason: { validate: (state: IServerState) => hasNoSeason(state) },
  hasSeason: { validate: (state: IServerState) => hasSeason(state) },
  hasUpcomingMovieNight: { validate: (state: IServerState) => hasUpcomingMovieNight(state) },
  hasVotes: (user: IUser) => ({ validate: (state: IServerState) => hasVotes(state, user) }),
  isInVotingPhase: (phase: VotingPhase) => ({ validate: (state: IServerState) => isInVotingPhase(state, phase) }),
  isNotVoting: { validate: (state: IServerState) => isNotVoting(state) },
  isVoteable: (item: IIdenifiable) => ({ validate: (state: IServerState) => isVotable(state, item) }),
  isVoting: { validate: (state: IServerState) => isVoting(state) },
  notNominated: (item: IIdenifiable) => ({ validate: (state: IServerState) => notNominated(state, item) }),
  userIsNotRegistered: (user: IUser) => ({ validate: (state: IServerState) => userIsNotRegistered(state, user) }),
  userIsRegistered: (user: IUser) => ({ validate: (state: IServerState) => userIsRegistered(state, user) }),
};

function hasNoSeason(state: IServerState): IValidationResult {
  if (typeof state.currentSeason !== 'undefined' &&
      state.currentSeason) {
    return {
      message: messages.seasonInProgress,
      success: false,
    };
  }
  return {
    success: true,
  };
}

export function hasSeason(state: IServerState): IValidationResult {
  if (typeof state.currentSeason === 'undefined' ||
      !state.currentSeason) {
    return {
      message: messages.seasonNotInProgress,
      success: false,
    };
  }
  return {
    success: true,
  };
}

export function hasMovieNight(state: IServerState): IValidationResult {
  if (typeof state.currentMovieNight === 'undefined' &&
      !state.currentMovieNight) {
    return {
      message: messages.noMovieNight,
      success: false,
    };
  }
  return {
    success: true,
  };
}

export function hasUpcomingMovieNight(state: IServerState): IValidationResult {
  if ((typeof state.currentMovieNight === 'undefined' && !state.currentMovieNight) ||
      state.currentMovieNight.night.getDate() < new Date().getDate()) {
    return {
      message: messages.noMovieNight,
      success: false,
    };
  }
  return {
    success: true,
  };
}

function isNotVoting(state: IServerState): IValidationResult {
  if (typeof state.voting !== 'undefined' &&
      state.voting) {
    return {
      message: messages.isElection,
      success: false,
    };
  }
  return {
    success: true,
  };
}

function isVoting(state: IServerState): IValidationResult {
  if (typeof state.voting === 'undefined' &&
      !state.voting) {
    return {
      message: messages.noElection,
      success: false,
    };
  }
  return {
    success: true,
  };
}

function isInVotingPhase(state: IServerState, phase: VotingPhase): IValidationResult {
  if (state.voting.phase !== phase) {
    return {
      message: messages.notInPhase(phase),
      success: false,
    };
  }
  return {
    success: true,
  };
}

function userIsNotRegistered(state: IServerState, user: IUser): IValidationResult {
  const foundUser = state.voting.voters.find(
    (voter) => {
    return voter.user.id === user.id;
  });

  if (foundUser) {
    return {
      message: messages.alreadyRegistered,
      success: false,
    };
  }
  return {
    success: true,
  };
}

function userIsRegistered(state: IServerState, user: IUser): IValidationResult {
  const foundUser = state.voting.voters.find(
    (voter) => {
    return voter.user.id === user.id;
  });

  if (!foundUser) {
    return {
      message: messages.notRegisteredToVote,
      success: false,
    };
  }
  return {
    success: true,
  };
}

function notNominated(state: IServerState, item: IIdenifiable): IValidationResult {
  if (state.voting.nominations.find(
    (nomination) => {
      return nomination.item.id === item.id;
    },
  )) {
    return {
      message: messages.alreadyNominated(item.id),
      success: false,
    };
  }
  return {
    success: true,
  };
}

function hasMinimumVoters(state: IServerState): IValidationResult {
  const voterLimit = state?.preferences?.voterLimit ? state.preferences.voterLimit : defaultVoterLimit;

  if (state.voting.voters.length < voterLimit) {
    return {
      message: messages.notEnoughVoters(state.voting.voters.length.toString(), voterLimit.toString()),
      success: false,
    };
  }
  return {
    success: true,
  };
}

function hasMinimumNominations(state: IServerState): IValidationResult {
  const nominationLimit = state?.preferences?.nominationLimit ?
    state.preferences.nominationLimit : defaultNominationLimit;

  if (state.voting.nominations.length < nominationLimit) {
    return {
      message: messages.notEnoughNominations(state.voting.nominations.length.toString(), nominationLimit.toString()),
      success: false,
    };
  }
  return {
    success: true,
  };
}

function checkStackedVoting(state: IServerState, item: IIdenifiable, user: IUser): IValidationResult {
  let allowed = false;
  if (state.preferences) {
    allowed = state.preferences.allowStackedVoting;
  }

  if (allowed) {
    return {
      success: true,
    };
  }

  const voteable = state.voting.votingRounds[state.voting.round].items.find(
    (voteableItem) => {
      if (voteableItem.item && voteableItem.item.id) {
        return voteableItem.item.id === item.id;
      }
    });
  if (voteable && voteable.voters && voteable.voters.find(
    (itemVoter) => {
      if (itemVoter.user && itemVoter.user.id) {
        return itemVoter.user.id === user.id;
      }
    })) {
    return {
      message: messages.alreadyVoted(item.id),
      success: false,
    };
  }

  return {
    success: true,
  };
}

function isVotable(state: IServerState, item: IIdenifiable): IValidationResult {
  const foundVotable = state.voting.votingRounds[state.voting.round].items.find(
    (voteableItem) => {
      return voteableItem.item.id === item.id;
    });
  if (!foundVotable) {
    return {
      message: messages.notVotable(item.id),
      success: false,
    };
  }
  return {
    success: true,
  };
}

function hasVotes(state: IServerState, user: IUser): IValidationResult {
  const savedUser = state.voting.votingRounds[state.voting.round].voters.find(
    (voter) => {
      return voter.user.id === user.id;
  });

  if (!savedUser || savedUser.points <= 0) {
    return {
      message: messages.notEnoughVotes,
      success: false,
    };
  }
  return {
    success: true,
  };
}
