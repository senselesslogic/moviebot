import { IServerState } from '../..';
import { IMovieLocator } from '../../../database';
import { Validator } from './..';
import { IMovieRepository } from './../../../database/interfaces';
import { MovieValidators } from './../movies';
import { IValidationResult } from './../validator';

describe('Movies validator', () => {
  const serverState: IServerState = { locatorId: "test" };
  const movieLocator: IMovieLocator = { id: "test" };

  describe('isMovie', () => {
    it('should fail when no movie is found.', () => {
      const validation = new Validator(serverState);

      const Mock = jest.fn<IMovieRepository, []>();
      const movieRepo: IMovieRepository = new Mock();

      validation.addValidator(MovieValidators.isMovie(undefined, movieRepo));

      const result: IValidationResult = validation.runValidation();

      expect(result.success).toBeFalsy();
    });

    it('should fail when movie repo throws an error.', () => {
      const validation = new Validator(serverState);

      const movieRepoMock = jest.fn<IMovieRepository, []>(() => ({
        get: () => {throw new Error(); },
        ...movieRepoMock,
      }));
      const movieRepo: IMovieRepository = new movieRepoMock();

      validation.addValidator(MovieValidators.isMovie(movieLocator.id, movieRepo));

      const result: IValidationResult = validation.runValidation();

      expect(result.success).toBeFalsy();
    });

    it('should pass when movie is found', () => {
      const validation = new Validator(serverState);

      const movieRepoMock = jest.fn<IMovieRepository, []>(() => ({
        get: jest.fn(),
        ...movieRepoMock,
      }));
      const movieRepo: IMovieRepository = new movieRepoMock();

      validation.addValidator(MovieValidators.isMovie(movieLocator.id, movieRepo));

      const result: IValidationResult = validation.runValidation();

      expect(result.success).toBeTruthy();
    });
  });
});
