import { IServerState } from '../..';
import { IIdenifiable, IUser, IVoteable, IVoter, IVoting } from '../../interfaces/sessionState';
import { Validator, VotingValidators } from './..';
import { VotingPhase } from './../../interfaces/sessionState';

describe('voting validator', () => {
  describe('hasMovieNight', () => {
    const testValidator = VotingValidators.hasMovieNight;
    const today = new Date();

    it('should succeed if server has movie night set.', () => {
      const serverState: IServerState = {
        currentMovieNight: {
          genre: {
            name: "any",
          },
          movie: {
            genres: [],
            id: "a",
            name: "test",
            year: 1,
          },
          night: today,
        },
        locatorId: 'test',
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });

    it('should fail if server does not have movie night set.', () => {
      const serverState: IServerState = {
        locatorId: 'test',
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeFalsy();
    });
  });

  describe('hasUpcomingMovieNight', () => {
    const testValidator = VotingValidators.hasUpcomingMovieNight;
    const today = new Date();
    const tomorrow = new Date(new Date().setDate(today.getDate() + 1));
    const yesterday = new Date(new Date().setDate(today.getDate() - 1));

    it('should succeed if server has movie night set with date that hasn\'t happened yet.', () => {
      const serverState: IServerState = {
        currentMovieNight: {
          genre: {
            name: "any",
          },
          movie: {
            genres: [],
            id: "a",
            name: "test",
            year: 1,
          },
          night: tomorrow,
        },
        locatorId: 'test',
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });

    it('should succeed if server has movie night set with current date.', () => {
      const serverState: IServerState = {
        currentMovieNight: {
          genre: {
            name: "any",
          },
          movie: {
            genres: [],
            id: "a",
            name: "test",
            year: 1,
          },
          night: today,
        },
        locatorId: 'test',
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });

    it('should fail if server has movie night set with date that has already happened.', () => {
      const serverState: IServerState = {
        currentMovieNight: {
          genre: {
            name: "any",
          },
          movie: {
            genres: [],
            id: "a",
            name: "test",
            year: 1,
          },
          night: yesterday,
        },
        locatorId: 'test',
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeFalsy();
    });

    it('should fail if server does not have movie night set.', () => {
      const serverState: IServerState = {
        locatorId: 'test',
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeFalsy();
    });
  });

  describe('hasNoSeason', () => {
    const testValidator = VotingValidators.hasNoSeason;

    it('should succeed if server has no season.', () => {
      const serverState: IServerState = { locatorId: 'test' };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });

    it('should fail if server has a season.', () => {
      const serverState: IServerState = {
        currentSeason: {
          genres: [],
          start: new Date(),
        },
        locatorId: 'test',
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeFalsy();
    });
  });

  describe('hasSeason', () => {
    const testValidator = VotingValidators.hasSeason;

    it('should fail if server has no season.', () => {
      const serverState: IServerState = {
        currentSeason: undefined,
        locatorId: 'test',
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeFalsy();
    });

    it('should succeed if server has a season.', () => {
      const serverState: IServerState = {
        currentSeason: {
          genres: [],
          start: new Date(),
        },
        locatorId: 'test',
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });
  });

  describe('isNotVoting', () => {
    const testValidator = VotingValidators.isNotVoting;

    it('should fail if server is voting.', () => {
      const votingMock = jest.fn<IVoting, []>();
      const serverState: IServerState = {
        locatorId: 'test',
        voting: new votingMock(),
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeFalsy();
    });

    it('should succeed if server is not voting.', () => {
      const serverState: IServerState = {
        locatorId: 'test',
        voting: undefined,
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });
  });

  describe('isVoting', () => {
    const testValidator = VotingValidators.isVoting;

    it('should fail if server is not voting.', () => {
      const serverState = {
        locatorId: 'test',
        voting: undefined,
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeFalsy();
    });

    it('should succeed if server is voting.', () => {
      const VotingMock = jest.fn<IVoting, []>();
      const serverState: IServerState = {
        locatorId: 'test',
        voting: new VotingMock(),
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });
  });

  describe('isInVotingPhase', () => {
    const testValidator = VotingValidators.isInVotingPhase(VotingPhase.voting);

    it('should fail if server is not in voting phase being validated.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        phase: VotingPhase.registration,
        ...VotingMock,
      }));
      const serverState: IServerState = {
        locatorId: 'test',
        voting: new VotingMock(),
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeFalsy();
    });

    it('should succeed if server is in voting phase being validated.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        phase: VotingPhase.voting,
        ...VotingMock,
      }));
      const serverState: IServerState = {
        locatorId: 'test',
        voting: new VotingMock(),
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });
  });

  describe('userIsNotRegistered', () => {
    const UserMock = jest.fn<IUser, []>(() => ({
      id: "1",
      ...UserMock,
    }));
    const testUser = new UserMock();
    const VoterMock = jest.fn<IVoter, []>(() => ({
      user: testUser,
      ...VoterMock,
    }));
    const testVoter = new VoterMock();
    const testValidator = VotingValidators.userIsNotRegistered(testUser);

    it('should fail if user is registered to vote.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        voters: [testVoter],
        ...VotingMock,
      }));
      const serverState: IServerState = {
        locatorId: 'test',
        voting: new VotingMock(),
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeFalsy();
    });

    it('should succeed if user is not registered to vote with no other voters registered.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        voters: [],
        ...VotingMock,
      }));
      const serverState: IServerState = {
        locatorId: 'test',
        voting: new VotingMock(),
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });

    it('should succeed if user is not registered to vote with at least one other voter registered.', () => {
      const DecoyUserMock = jest.fn<IUser, []>(() => ({
        id: "2",
        ...DecoyUserMock,
      }));
      const decoyTestUser = new DecoyUserMock();
      const DecoyVoterMock = jest.fn<IVoter, []>(() => ({
        user: decoyTestUser,
        ...DecoyVoterMock,
      }));
      const decoyVoter = new DecoyVoterMock();
      const VotingMock = jest.fn<IVoting, []>(() => ({
        voters: [decoyVoter],
        ...VotingMock,
      }));

      const serverState: IServerState = {
        locatorId: 'test',
        voting: new VotingMock(),
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });
  });

  describe('userIsRegistered', () => {
    const UserMock = jest.fn<IUser, []>(() => ({
      id: "1",
      ...UserMock,
    }));
    const testUser = new UserMock();
    const VoterMock = jest.fn<IVoter, []>(() => ({
      user: testUser,
      ...VoterMock,
    }));
    const testVoter = new VoterMock();

    const DecoyUserMock = jest.fn<IUser, []>(() => ({
      id: "2",
      ...DecoyUserMock,
    }));
    const decoyTestUser = new DecoyUserMock();
    const DecoyVoterMock = jest.fn<IVoter, []>(() => ({
      user: decoyTestUser,
      ...DecoyVoterMock,
    }));
    const decoyVoter = new DecoyVoterMock();

    const testValidator = VotingValidators.userIsRegistered(testUser);

    it('should fail if user is not registered to vote with no other users registered to vote.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        voters: [],
        ...VotingMock,
      }));
      const serverState: IServerState = {
        locatorId: 'test',
        voting: new VotingMock(),
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeFalsy();
    });

    it('should fail if user is not registered to vote with other users registered to vote.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        voters: [decoyVoter],
        ...VotingMock,
      }));
      const serverState: IServerState = {
        locatorId: 'test',
        voting: new VotingMock(),
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeFalsy();
    });

    it('should succeed if user is registered to vote with no other voters registered.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        voters: [testVoter],
        ...VotingMock,
      }));
      const serverState: IServerState = {
        locatorId: 'test',
        voting: new VotingMock(),
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });

    it('should succeed if user is registered to vote with at least one other voter registered.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        voters: [
          decoyVoter,
          testVoter,
        ],
        ...VotingMock,
      }));

      const serverState: IServerState = {
        locatorId: 'test',
        voting: new VotingMock(),
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });
  });

  describe('notNominated', () => {
    const ItemMock = jest.fn<IIdenifiable, []>(() => ({
      id: "1",
      ...ItemMock,
    }));
    const testItem = new ItemMock();
    const VoteableMock = jest.fn<IVoteable, []>(() => ({
      item: testItem,
      ...VoteableMock,
    }));
    const testVoteable = new VoteableMock();

    const DecoyItemMock = jest.fn<IIdenifiable, []>(() => ({
      id: "2",
      ...DecoyItemMock,
    }));
    const decoyTestItem = new DecoyItemMock();
    const DecoyVoteableMock = jest.fn<IVoteable, []>(() => ({
      item: decoyTestItem,
      ...DecoyVoteableMock,
    }));
    const decoyTestVoteable = new DecoyVoteableMock();

    const testValidator = VotingValidators.notNominated(testItem);

    it('should fail if item is nominated with no other items nominated.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        nominations: [testVoteable],
        ...VotingMock,
      }));
      const serverState: IServerState = {
        locatorId: 'test',
        voting: new VotingMock(),
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeFalsy();
    });

    it('should fail if item is nominated with at least one other item nominated.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        nominations: [
          decoyTestVoteable,
          testVoteable,
        ],
        ...VotingMock,
      }));
      const serverState: IServerState = {
        locatorId: 'test',
        voting: new VotingMock(),
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeFalsy();
    });

    it('should succeed if item is not nominated with no other items nominated.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        nominations: [],
        ...VotingMock,
      }));
      const serverState: IServerState = {
        locatorId: 'test',
        voting: new VotingMock(),
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });

    it('should succeed if item is not nominated with at least one other item nominated.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        nominations: [decoyTestVoteable],
        ...VotingMock,
      }));
      const serverState: IServerState = {
        locatorId: 'test',
        voting: new VotingMock(),
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });
  });

  describe('hasMinimumVoters', () => {
    const testValidator = VotingValidators.hasMinimumVoters;

    const VoterMock = jest.fn<IVoter, []>();
    const testVoter = new VoterMock();
    const otherTestVoter = new VoterMock();

    it('should fail if the number of votes are less than the minimum.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        voters: [],
        ...VotingMock,
      }));
      const testVoting = new VotingMock();
      const serverState: IServerState = {
        locatorId: 'test',
        preferences: {
          voterLimit: 1,
        },
        voting: testVoting,
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeFalsy();
    });

    it('should succeed if the number of voters meet the minimum.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        voters: [testVoter],
        ...VotingMock,
      }));
      const testVoting = new VotingMock();
      const serverState: IServerState = {
        locatorId: 'test',
        preferences: {
          voterLimit: 1,
        },
        voting: testVoting,
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });

    it('should succeed if the number of voters is above the minimum.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        voters: [
          testVoter,
          otherTestVoter,
        ],
        ...VotingMock,
      }));
      const testVoting = new VotingMock();
      const serverState: IServerState = {
        locatorId: 'test',
        preferences: {
          voterLimit: 1,
        },
        voting: testVoting,
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });
  });

  describe('hasMinimumNominations', () => {
    const testValidator = VotingValidators.hasMinimumNominations;

    const VoteableMock = jest.fn<IVoteable, []>();
    const testNomination = new VoteableMock();
    const otherTestNomination = new VoteableMock();

    it('should fail if the number of nominations are less than the minimum.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        nominations: [],
        ...VotingMock,
      }));
      const testVoting = new VotingMock();
      const serverState: IServerState = {
        locatorId: 'test',
        preferences: {
          nominationLimit: 1,
        },
        voting: testVoting,
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeFalsy();
    });

    it('should succeed if the number of nominations meet the minimum.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        nominations: [testNomination],
        ...VotingMock,
      }));
      const testVoting = new VotingMock();
      const serverState: IServerState = {
        locatorId: 'test',
        preferences: {
          nominationLimit: 1,
        },
        voting: testVoting,
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });

    it('should succeed if the number of nominations is above the minimum.', () => {
      const VotingMock = jest.fn<IVoting, []>(() => ({
        nominations: [
          testNomination,
          otherTestNomination,
        ],
        ...VotingMock,
      }));
      const testVoting = new VotingMock();
      const serverState: IServerState = {
        locatorId: 'test',
        preferences: {
          nominationLimit: 1,
        },
        voting: testVoting,
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });
  });

  describe('checkStackedVoting', () => {
    const ItemMock = jest.fn<IIdenifiable, []>(() => ({
      id: "1",
      ...ItemMock,
    }));
    const UserMock = jest.fn<IUser, []>(() => ({
      id: "a",
      ...UserMock,
    }));

    const testItem = new ItemMock();
    const testUser = new UserMock();

    const VoterMock = jest.fn<IVoter, []>(() => ({
      points: 1,
      user: testUser,
    }));

    const testVoter = new VoterMock();

    const testValidator = VotingValidators.checkStackedVoting(testItem, testUser);

    it('should fail if stacked voting is not allowed and user has already voted for item.', () => {
      const VoteableMock = jest.fn<IVoteable, []>(() => ({
        item: testItem,
        voters: [testVoter],
        ...VoteableMock,
      }));

      const testVotable = new VoteableMock();

      const VotingMock = jest.fn<IVoting, []>(() => ({
        round: 0,
        votingRounds: [{
          items: [testVotable],
        }],
        ...VotingMock,
      }));
      const testVoting = new VotingMock();
      const serverState: IServerState = {
        locatorId: 'test',
        preferences: {
          allowStackedVoting: false,
        },
        voting: testVoting,
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeFalsy();
    });

    it('should succeed if stacked voting is allowed and user has already voted for item.', () => {
      const VoteableMock = jest.fn<IVoteable, []>(() => ({
        item: testItem,
        voters: [testVoter],
        ...VoteableMock,
      }));

      const testVotable = new VoteableMock();

      const VotingMock = jest.fn<IVoting, []>(() => ({
        round: 0,
        votingRounds: [{
          items: [testVotable],
        }],
        ...VotingMock,
      }));
      const testVoting = new VotingMock();
      const serverState: IServerState = {
        locatorId: 'test',
        preferences: {
          allowStackedVoting: true,
        },
        voting: testVoting,
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });

    it('should succeed if stacked voting is not allowed and user has not already voted for item.', () => {
      const VoteableMock = jest.fn<IVoteable, []>(() => ({
        item: testItem,
        voters: [],
        ...VoteableMock,
      }));

      const testVotable = new VoteableMock();

      const VotingMock = jest.fn<IVoting, []>(() => ({
        round: 0,
        votingRounds: [{
          items: [testVotable],
        }],
        ...VotingMock,
      }));
      const testVoting = new VotingMock();
      const serverState: IServerState = {
        locatorId: 'test',
        preferences: {
          allowStackedVoting: false,
        },
        voting: testVoting,
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });

    it('should succeed if stacked voting is allowed and user has not already voted for item.', () => {
      const VoteableMock = jest.fn<IVoteable, []>(() => ({
        item: testItem,
        voters: [],
        ...VoteableMock,
      }));

      const testVotable = new VoteableMock();

      const VotingMock = jest.fn<IVoting, []>(() => ({
        round: 0,
        votingRounds: [{
          items: [testVotable],
        }],
        ...VotingMock,
      }));
      const testVoting = new VotingMock();
      const serverState: IServerState = {
        locatorId: 'test',
        preferences: {
          allowStackedVoting: true,
        },
        voting: testVoting,
      };
      const validation = new Validator(serverState);

      validation.addValidator(testValidator);

      const result = validation.runValidation();

      expect(result.success).toBeTruthy();
    });
  });
});
