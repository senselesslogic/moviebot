import { IServerState } from '../..';
import { Validator } from './../validator';

describe('validator', () => {
  it('should fail if no validators are added.', () => {
    const serverState: IServerState = { locatorId: 'test' };
    const validator = new Validator(serverState);

    const result = validator.runValidation();

    expect(result.success).toBeFalsy();
  });
});
