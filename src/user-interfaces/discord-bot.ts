import * as Discord from 'discord.js';
import { execute, IServerStateLocator } from '../core';
import { IResponseMessage } from '../core/message';
import { handleMessage } from '../utils';
import { IMovieRepository, IStateRepository } from './../database';

export class DiscordBot {
  private client = new Discord.Client();

  constructor(private discordToken: string, private repository: IStateRepository, private movieRepo: IMovieRepository) {
    this._registerEvents();
    this._login(this.discordToken);
  }

  private _registerEvents() {
    this.client.on('ready', () => {
      console.log(`logged in as ${this.client.user.tag}!`);
    });

    this.client.on('message', async (message: Discord.Message) => {
      const parsed = handleMessage(message.content);
      if (parsed && parsed.length > 0) {
        let guildId: string;
        if (message.guild &&
            message.guild.id) {
          guildId = message.guild.id;
        } else {
          const userState = await this.repository.getUserState({locatorId: message.author.id});
          if (userState && userState.registration) {
            guildId = userState.registration.locatorId;
          } else {
            message.author.sendMessage("You are not registered with a guild.");
            return;
          }
        }

        const identifier: IServerStateLocator = {
          locatorId: guildId,
        };

        const response: IResponseMessage = await execute({
          broadcastId: message.channel.id,
          movieRepo: this.movieRepo,
          repo: this.repository,
          serverLocator: identifier,
          user: {
            id: message.author.id,
          },
          userCommand: parsed,
        });

        this._passResponseAlong(response, message);
      }
    });
  }

  private _passResponseAlong(response: IResponseMessage, message: Discord.Message) {
    if (response.reply) {
      message.reply(response.reply);
    }
    if (response.broadcast) {
      let broadcastChannel: Discord.Channel;
      if (message.channel.id === response.broadcast.channelId) {
        broadcastChannel = message.channel;
      } else {
        broadcastChannel = message.guild.channels.find((item) => {
          return item.id === response.broadcast.channelId;
        });
        if (!broadcastChannel) {
          broadcastChannel = message.channel;
        }
      }
      message.channel.sendMessage(response.broadcast.message);
    }
  }

  private _login(token: string) {
    this.client.login(token);
  }
}
