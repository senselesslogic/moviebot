import { execute, IServerStateLocator, IUser } from "../core";
import { IResponseMessage } from "../core/message";
import { IMovieRepository, IStateRepository } from '../database';
import { messages } from '../utils';

const output = {
  reply: (message) => { console.log(message); },
};

export function Cli(
  command: string[],
  repository: IStateRepository,
  locator: IServerStateLocator,
  user: IUser,
  movieRepo: IMovieRepository,
) {
  if (command && command.length > 0) {
    execute({
      broadcastId: "0",
      movieRepo,
      repo: repository,
      serverLocator: locator,
      user,
      userCommand: command,
    }).then((response: IResponseMessage) => {
      if(response.broadcast) {
        output.reply("Public: " + response.broadcast);
      }
      if (response.reply) {
        output.reply("Author: " + response.reply);
      }
      repository.close();
    }).catch((err) => {
      output.reply(err);
    });
  } else {
    output.reply(messages.commandNotFound(command.join(' ')));
  }
}
