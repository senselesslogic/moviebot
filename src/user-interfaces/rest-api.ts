import * as bodyParser from 'body-parser';
import * as express from 'express';
import { execute, IServerStateLocator, IIdenifiable } from '../core';
import { IMovieRepository, IStateRepository } from './../database';

export interface IRestCommand {
  Command: string[];
  ServerId: string;
  UserId: string;
}

export class RestApiServer {

    private readonly SERVER_STARTED = 'Server started on port: ';
    private logger = {
      log: (message: string) => { console.log(message); },
    };
    private app = express();

    constructor(private repository: IStateRepository, private movieRepo: IMovieRepository) {
      this.app.use(bodyParser.json());
      this.app.use(bodyParser.urlencoded({extended: true}));
      this.registerRoutes();
    }

    public start(port: number): void {
      this.app.get('*', (req, res) => {
          res.send(this.SERVER_STARTED + port);
      });
      this.app.listen(port, () => {
          this.logger.log(this.SERVER_STARTED + port);
      });
    }

    private registerRoutes() {
      this.app.post('/', (req, res) => {
        res.send('Hello wonderful person!');
      });
      this.app.post('/command', (req, res) => {
        const body: IRestCommand = req.body;
        if (body.ServerId === null) {
          res.send('ServerId not found.');
        }
        if (body.UserId === null) {
          res.send('UserId not found.');
        }
        if (body.Command === null) {
          res.send('Command not found.');
        }
        execute({
          broadcastId: "0",
          movieRepo: this.movieRepo,
          repo: this.repository,
          serverLocator: {
            locatorId: body.ServerId,
          },
          user: {
            id: body.UserId,
          },
          userCommand: body.Command,
        }).then((response) => {
          if (response.reply) {
            res.send(response.reply);
          }
          if (response.broadcast) {
            res.send(response.broadcast.message);
          }
        }).catch((error) => {
          this.logger.log(error);
        });
      });
    }
}
