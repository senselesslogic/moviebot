import { Collection, Db, MongoClient } from 'mongodb';
import config from '../config';
import {
  IServerState,
  IServerStateLocator,
  IUserLocator,
  IUserState,
} from '../core';
import { IStateRepository } from './';

export class MongoRepository implements IStateRepository {
  private almostClient: Promise<MongoClient>;
  private client: MongoClient;
  private db: Db;
  private col: Collection<any>;

  constructor() {
    this.almostClient = MongoClient.connect(config.db.url, { useNewUrlParser: true });
  }

  public async getServerState(locator: IServerStateLocator): Promise<IServerState> {
    return await this._execAgainstSessionStateCollection(async (col: Collection) => {
      let result = await col.findOne(locator);
      if (!result) {
        const insertResult = await col.insertOne({
          created: Date.now(),
          ...locator,
        });
        result = await col.findOne({ _id: insertResult.insertedId });
      }
      return result;
    });
  }

  public async setServerState(state: IServerState): Promise<IServerState> {
    return await this._execAgainstSessionStateCollection(async (col: Collection) => {
      const updateResult = await col.updateOne({ locatorId: state.locatorId }, { $set: state });
      const result = await col.findOne({ _id: updateResult.upsertedId });
      return result;
    });
  }

  public async getUserState(locator: IUserLocator): Promise<IUserState> {
    return await this._execAgainstUserStateCollection(async (col: Collection) => {
      let result = await col.findOne(locator);
      if (!result) {
        const insertResult = await col.insertOne({
          created: Date.now(),
          ...locator,
        });
        result = await col.findOne({ _id: insertResult.insertedId });
      }
      return result;
    });
  }

  public async setUserState(state: IUserState): Promise<IUserState> {
    return await this._execAgainstUserStateCollection(async (col: Collection) => {
      const updateResult = await col.updateOne({ locatorId: state.locatorId }, { $set: state });
      const result = await col.findOne({ _id: updateResult.upsertedId });
      return result;
    });
  }

  public async close(): Promise<void> {
    if (typeof this.client !== 'undefined') {
      await this.client.close();
    }
  }

  private async _execAgainstSessionStateCollection(
      execFunc: (col: Collection) => Promise<IServerState>): Promise<IServerState> {
    return await this._execAgainstCollection(config.db.sessionStateCollection, execFunc);
  }

  private async _execAgainstUserStateCollection(
      execFunc: (col: Collection) => Promise<IServerState>): Promise<IUserState> {
    return await this._execAgainstCollection(config.db.userCollection, execFunc);
  }

  private async _execAgainstCollection(
      collection: string,
      execFunc: (col: Collection) => Promise<IServerState>): Promise<any> {
    if (typeof this.col !== 'undefined') {
      const funcResult = await execFunc(this.col);

      return funcResult;
    }
    this.client = await this.almostClient;
    this.db = this.client.db(config.db.name);
    this.col = this.db.collection(collection);

    const result = await execFunc(this.col);

    return result;
  }
}
