import * as imdb from 'imdb-api';
import {
  IServerState,
  IServerStateLocator,
  IUserLocator,
  IUserState,
} from './../core';

export interface IStateRepository {
  close: () => Promise<void>;
  getServerState: (locator: IServerStateLocator) => Promise<IServerState>;
  setServerState: (serverState: IServerState) => Promise<IServerState>;
  getUserState: (locator: IUserLocator) => Promise<IUserState>;
  setUserState: (userState: IUserState) => Promise<IUserState>;
}

export interface IMovieRepository {
  get: (locator: IMovieLocator) => Promise<imdb.Movie>;
  search: (searchQuery: IMovieSearch) => Promise<imdb.SearchResults>;
}

export interface IMovieLocator {
  id: string;
}

export interface IMovieSearch {
  title: string;
  year?: number;
}
