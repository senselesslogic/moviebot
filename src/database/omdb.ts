import * as imdb from 'imdb-api';
import { IMovieLocator, IMovieRepository, IMovieSearch } from './interfaces';

const apiTimeout = 10000;

export class Omdb implements IMovieRepository {
  private client: imdb.Client;

  public constructor(apiKey: string) {
    this.client = new imdb.Client({
      apiKey,
      timeout: apiTimeout,
    });
  }

  public async search(query: IMovieSearch): Promise<imdb.SearchResults> {
    return query.year ?
      this.client.search({ name: query.title, year: query.year }) : this.client.search({ name: query.title });
  }

  public async get(locator: IMovieLocator): Promise<imdb.Movie> {
    return this.client.get({ id: locator.id });
  }
}
