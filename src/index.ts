import * as Commander from 'commander';
import { MongoRepository } from './database';
import { Omdb } from './database/omdb';
import { secrets } from './secrets';
import { Cli, DiscordBot } from './user-interfaces';
import { RestApiServer } from './user-interfaces/rest-api';

Commander
  .version('1.0.0')
  .option('--cli', 'Command line interface.')
  .option('--restapi <a>', 'Rest API server interface.')
  .option('--locator-id <a>', 'Guild and Channel Ids.', '1')
  .option('--user-id <a>', 'User Id.', '1')
  .parse(process.argv);

const repo = new MongoRepository();
const movieRepo = new Omdb(secrets.omdbApiKey);

if (Commander.cli) {
  const args = process.argv.filter((e) => {
    return e !== '--cli';
  }).splice(2);
  Cli(
    args,
    repo,
    {
      locatorId: Commander.locatorId,
    },
    {
      id: Commander.userId,
    },
    movieRepo,
  );
} else if (Commander.restapi) {
  const server = new RestApiServer(repo, movieRepo);
  server.start(Commander.restapi);
} else {
  const DISCORD_TOKEN = secrets.discordBotToken;
  const bot = new DiscordBot(
    DISCORD_TOKEN,
    repo,
    movieRepo,
  );
  if (!bot) {
    console.error("Bot failed to init.");
  }
}
